from .widgets import *
from toga.style import Pack
from toga.style.pack import COLUMN

main_window_content = toga.Box(style=Pack(direction=COLUMN), children=[
    main_fill_box,
    main_checking_hbox0,
    main_checking_hbox1,
    main_books_hbox0,
    main_books_hbox1,
    main_books_hbox2,
    main_students_hbox0,
    main_students_hbox1,
    main_students_hbox2,
    main_database_hbox0,
    main_database_hbox1,
    main_fill_box
])

main_checking_hbox0.add(checking_label)
main_checking_hbox1.add(main_fill_box, check_out_button, check_in_button, main_fill_box)

main_books_hbox0.add(books_label)
main_books_hbox1.add(main_fill_box, add_book_button, retrieve_book_button, main_fill_box)
main_books_hbox2.add(main_fill_box, edit_book_button, delete_book_button, main_fill_box)

main_students_hbox0.add(students_label)
main_students_hbox1.add(main_fill_box, add_student_button, retrieve_student_button, main_fill_box)
main_students_hbox2.add(main_fill_box, edit_student_button, delete_student_button, main_fill_box)

main_database_hbox0.add(database_label)
main_database_hbox1.add(main_fill_box, database_sort_button, main_fill_box)


check_out_book_content = toga.Box(style=Pack(direction=COLUMN), children=[
    check_out_fill_box,
    check_out_hbox0,
    check_out_hbox1,
    check_out_hbox2,
    check_out_hbox3,
    check_out_fill_box
])

check_out_hbox0.add(check_out_label)
check_out_hbox1.add(check_out_fill_box, check_out_isbn_input, check_out_copy_no_input, check_out_fill_box)
check_out_hbox2.add(check_out_fill_box, check_out_student_input, check_out_fill_box)
check_out_hbox3.add(check_out_fill_box, check_out_submit_button, check_out_cancel_button, check_out_fill_box)

check_in_book_content = toga.Box(style=Pack(direction=COLUMN), children=[
    check_in_fill_box,
    check_in_hbox0,
    check_in_hbox1,
    check_in_hbox2,
    check_in_hbox3,
    check_in_fill_box
])

check_in_hbox0.add(check_in_label)
check_in_hbox1.add(check_in_fill_box, check_in_isbn_input, check_in_copy_no_input, check_in_fill_box)
check_in_hbox2.add(check_in_fill_box, check_in_student_input, check_in_fill_box)
check_in_hbox3.add(check_in_fill_box, check_in_submit_button, check_in_cancel_button, check_in_fill_box)


add_book_content = toga.Box(style=Pack(direction=COLUMN), children=[
    add_book_fill_box,
    add_book_hbox0,
    add_book_hbox1,
    add_book_hbox2,
    add_book_fill_box
])

add_book_hbox0.add(add_book_label)
add_book_hbox1.add(add_book_fill_box, add_book_isbn_input, add_book_fill_box)
add_book_hbox2.add(add_book_fill_box, add_book_submit_button, add_book_cancel_button, add_book_fill_box)


retrieve_book_content = toga.Box(style=Pack(direction=COLUMN), children=[
            retrieve_book_fill_box,
            retrieve_book_hbox0,
            retrieve_book_hbox1,
            retrieve_book_hbox2,
            retrieve_book_fill_box
        ])

retrieve_book_hbox0.add(retrieve_book_label)
retrieve_book_hbox1.add(retrieve_book_fill_box, retrieve_book_isbn_input, retrieve_book_copy_no, retrieve_book_fill_box)
retrieve_book_hbox2.add(retrieve_book_fill_box, retrieve_book_submit_button, retrieve_book_cancel_button, retrieve_book_fill_box)


edit_book_content = toga.Box(style=Pack(direction=COLUMN), children=[
    edit_book_fill_box,
    edit_book_hbox0,
    edit_book_hbox1,
    edit_book_hbox2,
    edit_book_fill_box
])

edit_book_hbox0.add(edit_book_label)
edit_book_hbox1.add(edit_book_fill_box, edit_book_vbox0, edit_book_fill_box)
edit_book_hbox2.add(edit_book_fill_box, edit_book_submit_button, edit_book_cancel_button, edit_book_fill_box)

edit_book_vbox0.add(edit_book_isbn_input, edit_book_title_text,
                    edit_book_authors_text, edit_book_lccn_text,
                    edit_book_lcc_text, edit_book_retrieve_button)


delete_book_content = toga.Box(style=Pack(direction=COLUMN), children=[
            delete_book_fill_box,
            delete_book_hbox0,
            delete_book_hbox1,
            delete_book_hbox2,
            delete_book_hbox3,
            delete_book_fill_box
        ])

delete_book_hbox0.add(delete_book_label)
delete_book_hbox1.add(delete_book_fill_box, delete_book_isbn_input, delete_book_copy_no, delete_book_fill_box)
delete_book_hbox2.add(delete_book_fill_box, delete_book_all_switch, delete_book_fill_box)
delete_book_hbox3.add(delete_book_fill_box, delete_book_submit_button, delete_book_cancel_button, delete_book_fill_box)


add_student_content = toga.Box(style=Pack(direction=COLUMN), children=[
            add_student_fill_box,
            add_student_hbox0,
            add_student_hbox1,
            add_student_hbox2,
            add_student_hbox3,
            add_student_hbox4,
            add_student_fill_box
])

add_student_hbox0.add(add_student_label)
add_student_hbox1.add(add_student_fill_box, add_student_student_input, add_student_fill_box)
add_student_hbox2.add(add_student_fill_box, add_student_first_name_input, add_student_fill_box)
add_student_hbox3.add(add_student_fill_box, add_student_last_name_input, add_student_fill_box)
add_student_hbox4.add(add_student_fill_box, add_student_submit_button, add_student_cancel_button, add_student_fill_box)


retrieve_student_content = toga.Box(style=Pack(direction=COLUMN), children=[
            retrieve_student_fill_box,
            retrieve_student_hbox0,
            retrieve_student_hbox1,
            retrieve_student_hbox2,
            retrieve_student_fill_box
        ])

retrieve_student_hbox0.add(retrieve_student_label)
retrieve_student_hbox1.add(retrieve_student_fill_box, retrieve_student_student_input, retrieve_student_fill_box)
retrieve_student_hbox2.add(retrieve_student_fill_box, retrieve_student_submit_button, retrieve_student_cancel_button, retrieve_student_fill_box)


edit_student_content = toga.Box(style=Pack(direction=COLUMN), children=[
            edit_student_fill_box,
            edit_student_hbox0,
            edit_student_hbox1,
            edit_student_hbox2,
            edit_student_fill_box
])

edit_student_hbox0.add(edit_student_label)
edit_student_hbox1.add(edit_student_fill_box, edit_student_vbox0, edit_student_fill_box)
edit_student_hbox2.add(edit_student_fill_box, edit_student_submit_button, edit_student_cancel_button, edit_student_fill_box)

edit_student_vbox0.add(edit_student_student_input,
                       edit_student_first_name_input,
                       edit_student_last_name_input,
                       edit_student_retrieve_button)


delete_student_content = toga.Box(style=Pack(direction=COLUMN), children=[
            delete_student_fill_box,
            delete_student_hbox0,
            delete_student_hbox1,
            delete_student_hbox2,
            delete_student_fill_box
        ])

delete_student_hbox0.add(delete_student_label)
delete_student_hbox1.add(delete_student_fill_box, delete_student_student_input, delete_student_fill_box)
delete_student_hbox2.add(delete_student_fill_box, delete_student_submit_button, delete_student_cancel_button, delete_student_fill_box)


database_sort_content = toga.Box(style=Pack(direction=COLUMN), children=[
            database_sort_hbox0,
            database_sort_hbox1,
            database_sort_hbox2
])

database_sort_hbox0.add(database_sort_label)
database_sort_hbox1.add(database_sort_table)
database_sort_hbox2.add(database_sort_fill_box, database_sort_cancel_button, database_sort_fill_box)