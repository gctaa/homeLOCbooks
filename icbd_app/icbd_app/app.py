import icbd_lib
import toga
from .widgets import *
from .windows import *
import os


class IcbdApplication(toga.App):

    def startup(self):
        self.main_window = toga.MainWindow(title=self.name)

        self.main_menu_window()

        def close_connections(*args):
            icbd_lib.close_connections()
            self.main_window._impl.on_close()

        self.main_window.on_close = close_connections

        self.main_window.show()

    def __set_window_content(self, content):
        self.main_window.content = content

    def main_menu_window(self, *args):

        check_out_button.on_press = self.check_out_book_window
        check_in_button.on_press = self.check_in_book_window
        add_book_button.on_press = self.add_book_window
        retrieve_book_button.on_press = self.retrieve_book_window
        edit_book_button.on_press = self.edit_book_window
        delete_book_button.on_press = self.delete_book_window
        add_student_button.on_press = self.add_student_window
        retrieve_student_button.on_press = self.retrieve_student_window
        edit_student_button.on_press = self.edit_student_window
        delete_student_button.on_press = self.delete_student_window
        database_sort_button.on_press = self.database_sort_window

        self.__set_window_content(main_window_content)

    def check_out_book_window(self, *args):
        check_out_isbn_input.enabled = True
        check_out_student_input.enabled = True
        check_out_submit_button.enabled = True
        check_out_copy_no_input.enabled = True
        check_out_isbn_input.value = ''
        check_out_copy_no_input.value = ''
        check_out_student_input.value = ''

        def backend_check_out_book(*args):
            isbn = remove_eol(check_out_isbn_input.value)
            student_id = check_out_student_input.value
            copy_no = check_out_copy_no_input.value
            
            check_out_submit_button.enabled = False
            check_out_student_input.enabled = False
            check_out_isbn_input.enabled = False
            check_out_copy_no_input.enabled = False

            try:
                icbd_lib.Book(isbn)
            except icbd_lib.exceptions.InvalidISBN:
                self.main_window.info_dialog('Error', 'The ISBN you entered is invalid.\n'
                                                      'Check the text field and try again.')
                check_out_isbn_input.enabled = True
                check_out_student_input.enabled = True
                check_out_submit_button.enabled = True
                check_out_copy_no_input.enabled = True
                return False

            student_id = check_student_id(student_id)
            if student_id is False:
                self.main_window.info_dialog('Error', 'Invalid student ID. Only non-null integer student\n'
                                                      'IDs are supported.')
                check_out_isbn_input.enabled = True
                check_out_student_input.enabled = True
                check_out_submit_button.enabled = True
                check_out_copy_no_input.enabled = True
                return False
            db = icbd_lib.Database()
            try:
                if check_out_copy_no_input.value != '':
                    copy_no = int(check_out_copy_no_input.value)
                else:
                    self.main_window.info_dialog('Error', 'Invalid text in book copy number.\n'
                                                          'Eenter a non-null integer.')
                    check_out_isbn_input.enabled = True
                    check_out_student_input.enabled = True
                    check_out_submit_button.enabled = True
                    check_out_copy_no_input.enabled = True
                    return False
            except ValueError:
                self.main_window.info_dialog('Error', 'Invalid text in book copy number.\n'
                                                      'Enter a non-null integer.')
                check_out_isbn_input.enabled = True
                check_out_student_input.enabled = True
                check_out_submit_button.enabled = True
                check_out_copy_no_input.enabled = True
                return False
            try:
                db.check_out(student_id, isbn, copy_no)
            except icbd_lib.exceptions.BookNotFound:
                self.main_window.info_dialog('Error', 'A book matching the provided data could not be found.')
                check_out_isbn_input.enabled = True
                check_out_student_input.enabled = True
                check_out_submit_button.enabled = True
                check_out_copy_no_input.enabled = True
                return False
            except icbd_lib.exceptions.NotEnoughInformation:
                self.main_window.info_dialog('Error', 'There are multiple books with the same ISBNs.\n'
                                                      'Provide a copy number in the text field next to the ISBN.')
                check_out_isbn_input.enabled = True
                check_out_student_input.enabled = True
                check_out_submit_button.enabled = True
                check_out_copy_no_input.enabled = True
                return False
            except icbd_lib.exceptions.TooMuchInformation:
                self.main_window.info_dialog('Error', 'There are multiple books with the same ISBN.\n'
                                                      'The copy you specified does not exist.')
                check_out_isbn_input.enabled = True
                check_out_student_input.enabled = True
                check_out_submit_button.enabled = True
                check_out_copy_no_input.enabled = True
                return False
            except icbd_lib.exceptions.LoanExists:
                self.main_window.info_dialog('Error', 'This book is already loaned to someone else.')
                check_out_isbn_input.enabled = True
                check_out_student_input.enabled = True
                check_out_submit_button.enabled = True
                check_out_copy_no_input.enabled = True
                return False
            except icbd_lib.exceptions.StudentNotFound:
                self.main_window.info_dialog('Error', 'A student with the specified ID could not be found.')
                check_out_isbn_input.enabled = True
                check_out_student_input.enabled = True
                check_out_submit_button.enabled = True
                check_out_copy_no_input.enabled = True
                return False
            check_out_isbn_input.enabled = True
            check_out_student_input.enabled = True
            check_out_submit_button.enabled = True
            check_out_copy_no_input.enabled = True
            check_out_isbn_input.value = ''
            check_out_copy_no_input.value = ''
            check_out_student_input.value = ''
        
        check_out_submit_button.on_press = backend_check_out_book
        check_out_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(check_out_book_content)

    def check_in_book_window(self, *args):
        check_in_isbn_input.enabled = True
        check_in_student_input.enabled = True
        check_in_submit_button.enabled = True
        check_in_copy_no_input.enabled = True
        check_in_isbn_input.value = ''
        check_in_copy_no_input.value = ''
        check_in_student_input.value = ''

        def backend_check_in_book(*args):
            isbn = remove_eol(check_in_isbn_input.value)
            student_id = check_in_student_input.value
            copy_no = check_in_copy_no_input.value
            
            check_in_submit_button.enabled = False
            check_in_student_input.enabled = False
            check_in_isbn_input.enabled = False
            check_in_copy_no_input.enabled = False

            try:
                icbd_lib.Book(isbn)
            except icbd_lib.exceptions.InvalidISBN:
                self.main_window.info_dialog('Error', 'The ISBN you entered is invalid.\n'
                                                      'Check the text field and try again.')
                check_in_isbn_input.enabled = True
                check_in_student_input.enabled = True
                check_in_submit_button.enabled = True
                check_in_copy_no_input.enabled = True
                return False

            student_id = check_student_id(student_id)
            if student_id is False:
                self.main_window.info_dialog('Error', 'Invalid student ID. Only non-null integer student\n'
                                                      'IDs are supported.')
                check_in_isbn_input.enabled = True
                check_in_student_input.enabled = True
                check_in_submit_button.enabled = True
                check_in_copy_no_input.enabled = True
                return False

            db = icbd_lib.Database()
            try:
                if check_in_copy_no_input.value != '':
                    copy_no = int(check_in_copy_no_input.value)
                else:
                    self.main_window.info_dialog('Error', 'Invalid text in book copy number.\n'
                                                          'Eenter a non-null integer.')
                    check_in_isbn_input.enabled = True
                    check_in_student_input.enabled = True
                    check_in_submit_button.enabled = True
                    check_in_copy_no_input.enabled = True
                    return False
            except ValueError:
                self.main_window.info_dialog('Error', 'Invalid text in book copy number.\n'
                                                      'Enter a non-null integer.')
                check_in_isbn_input.enabled = True
                check_in_student_input.enabled = True
                check_in_submit_button.enabled = True
                check_in_copy_no_input.enabled = True
                return False
            try:
                db.check_in(student_id, isbn, copy_no)
            except icbd_lib.exceptions.BookNotFound:
                self.main_window.info_dialog('Error', 'A book matching the provided data could not be found.')
                check_in_isbn_input.enabled = True
                check_in_student_input.enabled = True
                check_in_submit_button.enabled = True
                check_in_copy_no_input.enabled = True
                return False
            except icbd_lib.exceptions.NotEnoughInformation:
                self.main_window.info_dialog('Error', 'There are multiple books with the same ISBNs.\n'
                                                      'Provide a copy number in the text field next to the ISBN.')
                check_in_isbn_input.enabled = True
                check_in_student_input.enabled = True
                check_in_submit_button.enabled = True
                check_in_copy_no_input.enabled = True
                return False
            except icbd_lib.exceptions.TooMuchInformation:
                self.main_window.info_dialog('Error', 'There are multiple books with the same ISBN.\n'
                                                      'The copy you specified does not exist.')
                check_in_isbn_input.enabled = True
                check_in_student_input.enabled = True
                check_in_submit_button.enabled = True
                check_in_copy_no_input.enabled = True
                return False
            except icbd_lib.exceptions.LoanNotFound:
                self.main_window.info_dialog('Error', 'This book is not loaned to anyone.')
                check_in_isbn_input.enabled = True
                check_in_student_input.enabled = True
                check_in_submit_button.enabled = True
                check_in_copy_no_input.enabled = True
                return False
            except icbd_lib.exceptions.StudentNotFound:
                self.main_window.info_dialog('Error', 'A student with the specified ID could not be found.')
                check_in_isbn_input.enabled = True
                check_in_student_input.enabled = True
                check_in_submit_button.enabled = True
                check_in_copy_no_input.enabled = True
                return False
            check_in_isbn_input.enabled = True
            check_in_student_input.enabled = True
            check_in_submit_button.enabled = True
            check_in_copy_no_input.enabled = True
            check_in_isbn_input.value = ''
            check_in_copy_no_input.value = ''
            check_in_student_input.value = ''

        check_in_submit_button.on_press = backend_check_in_book
        check_in_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(check_in_book_content)

    def add_book_window(self, *args):

        add_book_isbn_input.value = ''
        add_book_submit_button.enabled = True
        add_book_cancel_button.enabled = True
        add_book_isbn_input.enabled = True

        def backend_add_book(*args):
            isbn = remove_eol(add_book_isbn_input.value)
            add_book_isbn_input.enabled = False
            add_book_submit_button.enabled = False
            try:
                book_to_add = icbd_lib.Book(isbn)
            except icbd_lib.exceptions.InvalidISBN:
                self.main_window.info_dialog('Error', 'The ISBN you entered is invalid.\n'
                                                      'Check the text field and try again.')
                add_book_isbn_input.enabled = True
                add_book_submit_button.enabled = True
                return False
            try:
                book_to_add.gather()
            except icbd_lib.exceptions.IncompleteData:
                pass
            db = icbd_lib.Database()
            try:
                db.create_book(book_to_add)
            except icbd_lib.exceptions.InvalidClass:
                self.main_window.info_dialog('Error', 'An unexpected error occured.\n'
                                                      'The book could not be added to the database.')
                return False
            add_book_isbn_input.enabled = True
            add_book_submit_button.enabled = True
            add_book_isbn_input.value = ''
            return True

        add_book_submit_button.on_press = backend_add_book
        add_book_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(add_book_content)

    def retrieve_book_window(self, *args):

        retrieve_book_isbn_input.value = ''
        retrieve_book_copy_no.value = ''
        retrieve_book_submit_button.enabled = True
        retrieve_book_cancel_button.enabled = True
        retrieve_book_isbn_input.enabled = True
        retrieve_book_copy_no.enabled = True

        def backend_retrieve_book(*args):
            isbn = remove_eol(retrieve_book_isbn_input.value)
            copy_no = retrieve_book_copy_no.value
            retrieve_book_isbn_input.enabled = False
            retrieve_book_submit_button.enabled = False
            retrieve_book_copy_no.enabled = False
            try:
                icbd_lib.Book(isbn)
            except icbd_lib.exceptions.InvalidISBN:
                self.main_window.info_dialog('Error', 'The ISBN you entered is invalid.\n'
                                                      'Check the text field and try again.')
                retrieve_book_isbn_input.enabled = True
                retrieve_book_submit_button.enabled = True
                return False
            db = icbd_lib.Database()
            try:
                if copy_no != '':
                    copy_no = int(copy_no)
                else:
                    copy_no = None
            except ValueError:
                self.main_window.info_dialog('Error', 'Invalid text in book copy number.\n'
                                                      'Leave the field empty or enter an integer.')
                retrieve_book_isbn_input.enabled = True
                retrieve_book_submit_button.enabled = True
                return False
            try:
                book_data = db.retrieve_book(isbn, copy_no=copy_no)
            except icbd_lib.exceptions.TooMuchInformation:
                self.main_window.info_dialog('Warning', 'A book with the provided ISBN has been found,\n'
                                                        'but the copy number could not be matched.')
                retrieve_book_isbn_input.enabled = True
                retrieve_book_submit_button.enabled = True
                return False
            except icbd_lib.exceptions.BookNotFound:
                self.main_window.info_dialog('Error', 'A book matching the provided data could not be found.')
                retrieve_book_isbn_input.enabled = True
                retrieve_book_submit_button.enabled = True
                return False
            except icbd_lib.exceptions.NotEnoughInformation:
                self.main_window.info_dialog('Error', 'There are multiple books with the same ISBNs.\n'
                                                      'Provide a copy number in the text field next to the ISBN.')
                retrieve_book_isbn_input.enabled = True
                retrieve_book_submit_button.enabled = True
                return False
            try:
                loan = db.find_loan_by_isbn_copy(isbn, copy_no)
                loaned_by = f'Student ID: {loan.student_id}\nCheck out: {loan.date_out}'
            except icbd_lib.exceptions.LoanNotFound:
                loaned_by = None
            if copy_no is None:
                copy_no = 'N/A'
            if loaned_by is None:
                loaned_by = 'N/A'
            self.main_window.info_dialog('Book data retrieved',
                                         f'Title: {book_data.title}\n'
                                         f'Authors: {book_data.authors}\n'
                                         f'ISBN: {isbn}\n'
                                         f'LCCN: {book_data.lccn}\n'
                                         f'LCC: {book_data.lcc}\n'
                                         f'Copy Number: {copy_no}\n\n'
                                         f'Loaned by: \n{loaned_by}')
            retrieve_book_isbn_input.enabled = True
            retrieve_book_submit_button.enabled = True
            retrieve_book_isbn_input.value = ''
            retrieve_book_copy_no.value = ''

        retrieve_book_submit_button.on_press = backend_retrieve_book
        retrieve_book_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(retrieve_book_content)

    def edit_book_window(self, *args):
        edit_book_isbn_input.value = ''
        edit_book_title_text.value = ''
        edit_book_authors_text.value = ''
        edit_book_lccn_text.value = ''
        edit_book_lcc_text.value = ''
        edit_book_isbn_input.enabled = True
        edit_book_title_text.enabled = False
        edit_book_authors_text.enabled = False
        edit_book_lccn_text.enabled = False
        edit_book_lcc_text.enabled = False
        edit_book_submit_button.enabled = False
        edit_book_retrieve_button.enabled = True

        def backend_edit_book_get(*args):
            isbn = remove_eol(edit_book_isbn_input.value)
            edit_book_isbn_input.enabled = False
            edit_book_retrieve_button.enabled = False
            try:
                icbd_lib.Book(isbn)
            except icbd_lib.exceptions.InvalidISBN:
                self.main_window.info_dialog('Error', 'The ISBN you entered is invalid.\n'
                                                      'Check the text field and try again.')
                edit_book_isbn_input.enabled = True
                edit_book_retrieve_button.enabled = True
                return False
            db = icbd_lib.Database()
            try:
                book_data = db.retrieve_book(edit_book_isbn_input.value, copy_no=1)
            except icbd_lib.exceptions.BookNotFound:
                self.main_window.info_dialog('Error', 'A book with the specified ISBN could not be found.')
                edit_book_isbn_input.enabled = True
                edit_book_retrieve_button.enabled = True
                return False
            edit_book_title_text.value = book_data.title
            edit_book_authors_text.value = book_data.authors
            edit_book_lccn_text.value = book_data.lccn
            edit_book_lcc_text.value = book_data.lcc
            edit_book_title_text.enabled = True
            edit_book_authors_text.enabled = True
            edit_book_lccn_text.enabled = True
            edit_book_lcc_text.enabled = True
            edit_book_submit_button.enabled = True

        def backend_edit_book_put(*args):
            isbn = remove_eol(edit_book_isbn_input.value)
            try:
                book_to_edit = icbd_lib.Book(isbn)
            except icbd_lib.exceptions.InvalidISBN:  # This should NEVER occur because the text field is not editable.
                self.main_window.info_dialog('Error', 'The ISBN you entered is invalid.\n'
                                                      'Check the text field and try again.')
                edit_book_isbn_input.value = ''
                edit_book_title_text.value = ''
                edit_book_authors_text.value = ''
                edit_book_lccn_text.value = ''
                edit_book_lcc_text.value = ''
                edit_book_isbn_input.enabled = True
                edit_book_title_text.enabled = False
                edit_book_authors_text.enabled = False
                edit_book_lccn_text.enabled = False
                edit_book_lcc_text.enabled = False
                edit_book_submit_button.enabled = False
                edit_book_retrieve_button.enabled = True
                return False
            book_to_edit.title = edit_book_title_text.value
            book_to_edit.authors = edit_book_authors_text.value
            book_to_edit.lccn = edit_book_lccn_text.value
            book_to_edit.lcc = edit_book_lcc_text.value
            db = icbd_lib.Database()
            try:
                db.edit_book(book_to_edit)
            except icbd_lib.exceptions.BookNotFound:  # This should NEVER occur because the text field is not editable.
                self.main_window.info_dialog('Error', 'A book with the specified ISBN could not be found.')
                edit_book_isbn_input.value = ''
                edit_book_title_text.value = ''
                edit_book_authors_text.value = ''
                edit_book_lccn_text.value = ''
                edit_book_lcc_text.value = ''
                edit_book_isbn_input.enabled = True
                edit_book_title_text.enabled = False
                edit_book_authors_text.enabled = False
                edit_book_lccn_text.enabled = False
                edit_book_lcc_text.enabled = False
                edit_book_submit_button.enabled = False
                edit_book_retrieve_button.enabled = True
                return False
            edit_book_isbn_input.value = ''
            edit_book_title_text.value = ''
            edit_book_authors_text.value = ''
            edit_book_lccn_text.value = ''
            edit_book_lcc_text.value = ''
            edit_book_isbn_input.enabled = True
            edit_book_title_text.enabled = False
            edit_book_authors_text.enabled = False
            edit_book_lccn_text.enabled = False
            edit_book_lcc_text.enabled = False
            edit_book_submit_button.enabled = False
            edit_book_retrieve_button.enabled = True

        edit_book_retrieve_button.on_press = backend_edit_book_get
        edit_book_submit_button.on_press = backend_edit_book_put
        edit_book_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(edit_book_content)

    def delete_book_window(self, *args):
        delete_book_isbn_input.enabled = True
        delete_book_submit_button.enabled = True
        delete_book_all_switch.enabled = True
        delete_book_isbn_input.value = ''
        delete_book_all_switch.is_on = False
        delete_book_copy_no.enabled = True
        delete_book_copy_no.value = ''

        def gui_update_copy_no_status(*args):
            if delete_book_all_switch.is_on:
                delete_book_copy_no.value = ''
                delete_book_copy_no.enabled = False
            else:
                delete_book_copy_no.enabled = True

        def backend_delete_book(*args):
            isbn = remove_eol(delete_book_isbn_input.value)
            copy_no = delete_book_copy_no.value
            delete_book_isbn_input.enabled = False
            delete_book_submit_button.enabled = False
            delete_book_all_switch.enabled = False
            delete_book_copy_no.enabled = False
            try:
                icbd_lib.Book(isbn)
            except icbd_lib.exceptions.InvalidISBN:
                self.main_window.info_dialog('Error', 'The ISBN you entered is invalid.\n'
                                                      'Check the text field and try again.')
                gui_update_copy_no_status()
                delete_book_isbn_input.enabled = True
                delete_book_submit_button.enabled = True
                delete_book_all_switch.enabled = True
                return False
            db = icbd_lib.Database()
            if copy_no == '' and delete_book_all_switch.is_on == False:
                copy_no = None
                self.main_window.info_dialog('Error', 'Specify a copy number if you are not\n'
                                                      'deleting all the books.')
                gui_update_copy_no_status()
                delete_book_isbn_input.enabled = True
                delete_book_submit_button.enabled = True
                delete_book_all_switch.enabled = True
                return False
            elif copy_no == '0' and delete_book_all_switch.is_on == False:
                self.main_window.info_dialog('Error', 'Invalid text in book copy number.\n'
                                                      'Enter an integer bigger than 0.')
                gui_update_copy_no_status()
                delete_book_isbn_input.enabled = True
                delete_book_submit_button.enabled = True
                delete_book_all_switch.enabled = True
                return False
            elif delete_book_all_switch.is_on == False:
                try:
                    copy_no = int(copy_no)
                except ValueError:
                    self.main_window.info_dialog('Error', 'Invalid text in book copy number.\n'
                                                          'Enter an integer bigger than 0.')
                    gui_update_copy_no_status()
                    delete_book_isbn_input.enabled = True
                    delete_book_submit_button.enabled = True
                    delete_book_all_switch.enabled = True
                    return False
            try:
                db.delete_book(str(delete_book_isbn_input.value), copy_no=copy_no, all=delete_book_all_switch.is_on)
            except icbd_lib.exceptions.BookNotFound:
                self.main_window.info_dialog('Error', 'A book with the specified data could not be found.')
                gui_update_copy_no_status()
                delete_book_isbn_input.enabled = True
                delete_book_submit_button.enabled = True
                delete_book_all_switch.enabled = True
                return False
            except icbd_lib.exceptions.NotEnoughInformation:
                self.main_window.info_dialog('Error', 'There are multiple books with the same ISBN.\n'
                                                      'Specify a copy number or check the box below\n'
                                                      'to delete all books with the specified ISBN.')
                gui_update_copy_no_status()
                delete_book_isbn_input.enabled = True
                delete_book_submit_button.enabled = True
                delete_book_all_switch.enabled = True
                return False
            except icbd_lib.exceptions.InUse:
                self.main_window.info_dialog('Error', 'This book is currently loaned out to someone.\n'
                                                      'Check in the book and try again.')
                gui_update_copy_no_status()
                delete_book_isbn_input.enabled = True
                delete_book_submit_button.enabled = True
                delete_book_all_switch.enabled = True
                return False
            delete_book_isbn_input.enabled = True
            delete_book_submit_button.enabled = True
            delete_book_all_switch.enabled = True
            delete_book_isbn_input.value = ''
            delete_book_copy_no.value = ''
            gui_update_copy_no_status()
            return True

        delete_book_submit_button.on_press = backend_delete_book
        delete_book_cancel_button.on_press = self.main_menu_window
        delete_book_all_switch.on_toggle = gui_update_copy_no_status

        self.__set_window_content(delete_book_content)

    def add_student_window(self, *args):
        add_student_student_input.value = ''
        add_student_first_name_input.value = ''
        add_student_last_name_input.value = ''
        add_student_student_input.enabled = True
        add_student_first_name_input.enabled = True
        add_student_last_name_input.enabled = True
        add_student_submit_button.enabled = True
        add_student_cancel_button.enabled = True

        def backend_add_student(*args):
            student_init = add_student_student_input.value
            first_name = add_student_first_name_input.value
            last_name = add_student_last_name_input.value
            add_student_student_input.enabled = False
            add_student_first_name_input.enabled = False
            add_student_last_name_input.enabled = False
            add_student_submit_button.enabled = False

            student_id = check_student_id(student_init)
            if student_id is False:
                self.main_window.info_dialog('Error', 'Invalid student ID. Only non-null integer student\n'
                                                      'IDs are supported.')
                add_student_student_input.enabled = True
                add_student_first_name_input.enabled = True
                add_student_last_name_input.enabled = True
                add_student_submit_button.enabled = True
                return False
            
            if any(i.isdigit() for i in first_name):
                self.main_window.info_dialog('Error', 'A student name should not contain any numbers.\n'
                                                      'Enter a valid first name.')
                add_student_student_input.enabled = True
                add_student_first_name_input.enabled = True
                add_student_last_name_input.enabled = True
                add_student_submit_button.enabled = True
                return False
            if any(i.isdigit() for i in last_name):
                self.main_window.info_dialog('Error', 'A student name should not contain any numbers.\n'
                                                      'Enter a valid last name.')
                add_student_student_input.enabled = True
                add_student_first_name_input.enabled = True
                add_student_last_name_input.enabled = True
                add_student_submit_button.enabled = True
                return False
            
            if first_name == '':
                self.main_window.info_dialog('Error', 'A student name cannot be empty.\n'
                                                      'Enter a valid first name.')
                add_student_student_input.enabled = True
                add_student_first_name_input.enabled = True
                add_student_last_name_input.enabled = True
                add_student_submit_button.enabled = True
                return False
            if last_name == '':
                self.main_window.info_dialog('Error', 'A student name cannot be empty.\n'
                                                      'Enter a valid last name.')
                add_student_student_input.enabled = True
                add_student_first_name_input.enabled = True
                add_student_last_name_input.enabled = True
                add_student_submit_button.enabled = True
                return False

            db = icbd_lib.Database()
            try:
                student = icbd_lib.Student(student_id, first_name, last_name)
                db.create_student(student)
            except icbd_lib.exceptions.InvalidClass:
                self.main_window.info_dialog('Error', 'An unexpected error occured.\n'
                                                      'The student could not be added to the database.')
                add_student_student_input.enabled = True
                add_student_first_name_input.enabled = True
                add_student_last_name_input.enabled = True
                add_student_submit_button.enabled = True
                return False
            except icbd_lib.exceptions.StudentExists:
                self.main_window.info_dialog('Error', 'A student with this ID already exists.')
                add_student_student_input.enabled = True
                add_student_first_name_input.enabled = True
                add_student_last_name_input.enabled = True
                add_student_submit_button.enabled = True
                return False
            
            add_student_student_input.value = ''
            add_student_first_name_input.value = ''
            add_student_last_name_input.value = ''
            add_student_student_input.enabled = True
            add_student_first_name_input.enabled = True
            add_student_last_name_input.enabled = True
            add_student_submit_button.enabled = True
            add_student_cancel_button.enabled = True

        add_student_submit_button.on_press = backend_add_student
        add_student_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(add_student_content)

    def retrieve_student_window(self, *args):
        retrieve_student_submit_button.enabled = True
        retrieve_student_student_input.enabled = True
        retrieve_student_student_input.value = ''
        
        def backend_retrieve_student(*args):
            student_init = retrieve_student_student_input.value
            retrieve_student_submit_button.enabled = False
            retrieve_student_student_input.enabled = False

            student_id = check_student_id(student_init)
            if student_id is False:
                self.main_window.info_dialog('Error', 'Invalid student ID. Only non-null integer student\n'
                                                      'IDs are supported.')
                retrieve_student_student_input.enabled = True
                retrieve_student_submit_button.enabled = True
                return False
            
            db = icbd_lib.Database()
            try:
                student_data = db.retrieve_student(student_id)
            except icbd_lib.exceptions.StudentNotFound:
                self.main_window.info_dialog('Error', 'A student with the specified ID could not be found.')
                retrieve_student_student_input.enabled = True
                retrieve_student_submit_button.enabled = True
                return False
            try:
                loan_list = db.find_loans_by_student_id(student_id)
            except icbd_lib.exceptions.LoanNotFound:
                loan_list = None
            if loan_list is None:
                loans = 'N/A'
            else:
                loans = ''
                for loan in loan_list:
                    loans += f'\nISBN: {loan.isbn} | Copy No: {loan.copy_no}\nCheck out: {loan.date_out}\n'
            self.main_window.info_dialog('Student data retrieved',
                                         f'Student ID: {student_data.student_id}\n'
                                         f'First Name: {student_data.first_name}\n'
                                         f'Last Name: {student_data.last_name}\n\n'
                                         f'Loaned books: {loans}')
            retrieve_student_submit_button.enabled = True
            retrieve_student_student_input.enabled = True
            retrieve_student_student_input.value = ''

        retrieve_student_submit_button.on_press = backend_retrieve_student
        retrieve_student_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(retrieve_student_content)

    def edit_student_window(self, *args):
        edit_student_student_input.value = ''
        edit_student_first_name_input.value = ''
        edit_student_last_name_input.value = ''
        edit_student_student_input.enabled = True
        edit_student_first_name_input.enabled = False
        edit_student_last_name_input.enabled = False
        edit_student_submit_button.enabled = False
        edit_student_retrieve_button.enabled = True

        def backend_edit_student_get(*args):
            student_init = edit_student_student_input.value
            student_id = check_student_id(student_init)
            edit_student_student_input.enabled = False
            edit_student_retrieve_button.enabled = False

            if student_id is False:
                self.main_window.info_dialog('Error', 'Invalid student ID. Only non-null integer student\n'
                                                      'IDs are supported.')
                edit_student_student_input.enabled = True
                edit_student_retrieve_button.enabled = True
                return False
            
            db = icbd_lib.Database()
            try:
                student_data = db.retrieve_student(student_id)
            except icbd_lib.exceptions.StudentNotFound:
                self.main_window.info_dialog('Error', 'A student with the specified ID could not be found.')
                edit_student_student_input.enabled = True
                edit_student_retrieve_button.enabled = True
                return False
            
            edit_student_first_name_input.value = student_data.first_name
            edit_student_last_name_input.value = student_data.last_name
            edit_student_first_name_input.enabled = True
            edit_student_last_name_input.enabled = True
            edit_student_submit_button.enabled = True

        def backend_edit_student_put(*args):
            student_init = edit_student_student_input.value
            first_name = edit_student_first_name_input.value
            last_name = edit_student_last_name_input.value
            student_id = check_student_id(student_init)
            edit_student_submit_button.enabled = False
            edit_student_first_name_input.enabled = False
            edit_student_last_name_input.enabled = False

            if student_id is False:  # This should NEVER happen because the text field is not editable.
                self.main_window.info_dialog('Error', 'Invalid student ID. Only non-null integer student\n'
                                                      'IDs are supported.')
                edit_student_student_input.enabled = True
                edit_student_retrieve_button.enabled = True
                edit_student_first_name_input.value = ''
                edit_student_last_name_input.value = ''
                edit_student_student_input.value = ''
                return False
            
            if any(i.isdigit() for i in first_name):
                self.main_window.info_dialog('Error', 'A student name should not contain any numbers.\n'
                                                      'Enter a valid first name.')
                edit_student_student_input.enabled = True
                edit_student_first_name_input.enabled = True
                edit_student_last_name_input.enabled = True
                edit_student_submit_button.enabled = True
                return False
            if any(i.isdigit() for i in last_name):
                self.main_window.info_dialog('Error', 'A student name should not contain any numbers.\n'
                                                      'Enter a valid last name.')
                edit_student_student_input.enabled = True
                edit_student_first_name_input.enabled = True
                edit_student_last_name_input.enabled = True
                edit_student_submit_button.enabled = True
                return False
            
            if first_name == '':
                self.main_window.info_dialog('Error', 'A student name cannot be empty.\n'
                                                      'Enter a valid first name.')
                edit_student_student_input.enabled = True
                edit_student_first_name_input.enabled = True
                edit_student_last_name_input.enabled = True
                edit_student_submit_button.enabled = True
                return False
            if last_name == '':
                self.main_window.info_dialog('Error', 'A student name cannot be empty.\n'
                                                      'Enter a valid last name.')
                edit_student_student_input.enabled = True
                edit_student_first_name_input.enabled = True
                edit_student_last_name_input.enabled = True
                edit_student_submit_button.enabled = True
                return False
            
            student = icbd_lib.Student(student_id, first_name, last_name)
            db = icbd_lib.Database()
            try:
                db.edit_student(student)
            except icbd_lib.exceptions.InvalidClass:
                self.main_window.info_dialog('Error', 'An unexpected error occured.\n'
                                                      'The student could not be added to the database.')
                edit_student_student_input.enabled = True
                edit_student_retrieve_button.enabled = True
                edit_student_first_name_input.value = ''
                edit_student_last_name_input.value = ''
                return False
            except icbd_lib.exceptions.StudentNotFound:  # This should NEVER occur because the text field is not editable.
                self.main_window.info_dialog('Error', 'A student with the specified ID could not be found.')
                edit_student_student_input.enabled = True
                edit_student_retrieve_button.enabled = True
                edit_student_first_name_input.value = ''
                edit_student_last_name_input.value = ''
                return False
            
            edit_student_student_input.enabled = True
            edit_student_retrieve_button.enabled = True
            edit_student_first_name_input.value = ''
            edit_student_last_name_input.value = ''
            edit_student_student_input.value = ''
        
        edit_student_retrieve_button.on_press = backend_edit_student_get
        edit_student_submit_button.on_press = backend_edit_student_put
        edit_student_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(edit_student_content)

    def delete_student_window(self, *args):
        delete_student_submit_button.enabled = True
        delete_student_student_input.enabled = True
        delete_student_student_input.value = ''

        def backend_delete_student(*args):
            student_init = delete_student_student_input.value
            delete_student_submit_button.enabled = False
            delete_student_student_input.enabled = False

            student_id = check_student_id(student_init)
            if student_id is False:
                self.main_window.info_dialog('Error', 'Invalid student ID. Only non-null integer student\n'
                                                      'IDs are supported.')
                delete_student_student_input.enabled = True
                delete_student_submit_button.enabled = True
                return False

            db = icbd_lib.Database()
            try:
                db.delete_student(student_id)
            except icbd_lib.exceptions.StudentNotFound:
                self.main_window.info_dialog('Error', 'A student with the specified ID could not be found.')
                delete_student_student_input.enabled = True
                delete_student_submit_button.enabled = True
                return False
            except icbd_lib.exceptions.InUse:
                self.main_window.info_dialog('Error', 'This student is currently loaning out books.\n'
                                                      'Check in the books and try again.')
                delete_student_student_input.enabled = True
                delete_student_submit_button.enabled = True
                return False
            delete_student_submit_button.enabled = True
            delete_student_student_input.enabled = True
            delete_student_student_input.value = ''

        delete_student_submit_button.on_press = backend_delete_student
        delete_student_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(delete_student_content)

    def database_sort_window(self, *args):
        database_sort_table.set_headings(['LCC', 'Title', 'Authors', 'LCCN', 'ISBN', 'Copy No'])
        db = icbd_lib.Database()
        book_sorted = db.sort_by_lcc()
        for book in book_sorted:
            _ = (book.lcc, book.title, book.authors, book.lccn, book.isbn, book.copy_no)
            database_sort_table.data.insert(0, book.lcc, book.title[:15], book.authors[:15], book.lccn, book.isbn, book.copy_no)

        database_sort_cancel_button.on_press = self.main_menu_window

        self.__set_window_content(database_sort_content)


def main():
    app_icon = os.path.join(os.getcwd(), "icons", "app_icon.png")
    return IcbdApplication('ICBD Application', 'com.gctaa.icbd_app', icon=app_icon)


def remove_eol(text):
    return text.strip()

def check_student_id(student_id):
    if '-' in student_id:
        return False
    try:
        return int(student_id)
    except ValueError:
        return False