import toga
from toga.style import Pack
from toga.style.pack import ROW, COLUMN, CENTER

# Constants
SUBMIT_TEXT = "Submit"
CANCEL_TEXT = "Cancel"
ISBN_TEXT = "Scan or enter ISBN"
STUDENT_TEXT = "Scan or enter Student ID"
COPY_TEXT = "#"

# Pack Styles
button_pack = Pack(padding=8, width=144)
large_button_pack = Pack(padding=8, width=304)
label_pack = Pack(text_align=CENTER, flex=1)
title_box_pack = Pack(direction=ROW, padding=8)
horizontal_box_pack = Pack(direction=ROW)
vertical_box_pack = Pack(direction=COLUMN)
text_pack = Pack(padding=8, width=304)
small_text_pack = Pack(padding=8, width=262)
copy_no_text_pack = Pack(padding=8, width=26)
switch_pack = Pack()


# Main Form===================================================================

# Checking Boxes
main_fill_box = toga.Box(style=Pack(flex=1))
main_checking_hbox0 = toga.Box(style=horizontal_box_pack)
main_checking_hbox1 = toga.Box(style=horizontal_box_pack)

# Checking Widgets
checking_label = toga.Label("Checking Operations", style=label_pack)
check_out_button = toga.Button("Check out", style=button_pack)
check_in_button = toga.Button("Check in", style=button_pack)

# Books Boxes
main_books_hbox0 = toga.Box(style=horizontal_box_pack)
main_books_hbox1 = toga.Box(style=horizontal_box_pack)
main_books_hbox2 = toga.Box(style=horizontal_box_pack)

# Books Widgets
books_label = toga.Label("Book Operations", style=label_pack)
add_book_button = toga.Button("Add Book", style=button_pack)
retrieve_book_button = toga.Button("Retrieve Book", style=button_pack)
edit_book_button = toga.Button("Edit Book", style=button_pack)
delete_book_button = toga.Button("Delete Book", style=button_pack)

# Students Boxes
main_students_hbox0 = toga.Box(style=horizontal_box_pack)
main_students_hbox1 = toga.Box(style=horizontal_box_pack)
main_students_hbox2 = toga.Box(style=horizontal_box_pack)

# Students Widgets
students_label = toga.Label("Student Operations", style=label_pack)
add_student_button = toga.Button("Add Student", style=button_pack)
retrieve_student_button = toga.Button("Retrieve Student", style=button_pack)
edit_student_button = toga.Button("Edit Student", style=button_pack)
delete_student_button = toga.Button("Delete Student", style=button_pack)

# Database Boxes
main_database_hbox0 = toga.Box(style=horizontal_box_pack)
main_database_hbox1 = toga.Box(style=horizontal_box_pack)

# Database Widgets
database_label = toga.Label("Database Operations", style=label_pack)
database_sort_button = toga.Button("Sort Database", style=large_button_pack)


# Check out Form==============================================================

# Check out Boxes
check_out_fill_box = toga.Box(style=Pack(flex=1))
check_out_hbox0 = toga.Box(style=horizontal_box_pack)
check_out_hbox1 = toga.Box(style=horizontal_box_pack)
check_out_hbox2 = toga.Box(style=horizontal_box_pack)
check_out_hbox3 = toga.Box(style=horizontal_box_pack)

# Check out Widgets
check_out_label = toga.Label("Check out", style=label_pack)
check_out_isbn_input = toga.TextInput(placeholder=ISBN_TEXT, style=small_text_pack)
check_out_copy_no_input = toga.TextInput(placeholder=COPY_TEXT, style=copy_no_text_pack)
check_out_student_input = toga.TextInput(placeholder=STUDENT_TEXT, style=text_pack)
check_out_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
check_out_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)


# Check in Form===============================================================

# Check out Boxes
check_in_fill_box = toga.Box(style=Pack(flex=1))
check_in_hbox0 = toga.Box(style=horizontal_box_pack)
check_in_hbox1 = toga.Box(style=horizontal_box_pack)
check_in_hbox2 = toga.Box(style=horizontal_box_pack)
check_in_hbox3 = toga.Box(style=horizontal_box_pack)

# Check out Widgets
check_in_label = toga.Label("Check in", style=label_pack)
check_in_isbn_input = toga.TextInput(placeholder=ISBN_TEXT, style=small_text_pack)
check_in_copy_no_input = toga.TextInput(placeholder=COPY_TEXT, style=copy_no_text_pack)
check_in_student_input = toga.TextInput(placeholder=STUDENT_TEXT, style=text_pack)
check_in_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
check_in_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)


# Add book Form===============================================================

# Add book Boxes
add_book_fill_box = toga.Box(style=Pack(flex=1))
add_book_hbox0 = toga.Box(style=horizontal_box_pack)
add_book_hbox1 = toga.Box(style=horizontal_box_pack)
add_book_hbox2 = toga.Box(style=horizontal_box_pack)

# Add book Widgets
add_book_label = toga.Label("Add book", style=label_pack)
add_book_isbn_input = toga.TextInput(placeholder=ISBN_TEXT, style=text_pack)
add_book_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
add_book_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)


# Retrieve book Form==========================================================

# Retrieve book Boxes
retrieve_book_fill_box = toga.Box(style=Pack(flex=1))
retrieve_book_hbox0 = toga.Box(style=horizontal_box_pack)
retrieve_book_hbox1 = toga.Box(style=horizontal_box_pack)
retrieve_book_hbox2 = toga.Box(style=horizontal_box_pack)

# Retrieve book Widgets
retrieve_book_label = toga.Label("Retrieve book", style=label_pack)
retrieve_book_isbn_input = toga.TextInput(placeholder=ISBN_TEXT, style=small_text_pack)
retrieve_book_copy_no = toga.TextInput(placeholder=COPY_TEXT, style=copy_no_text_pack)
retrieve_book_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
retrieve_book_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)


# Edit book Form==============================================================

# Edit book Boxes
edit_book_fill_box = toga.Box(style=Pack(flex=1))
edit_book_hbox0 = toga.Box(style=horizontal_box_pack)
edit_book_hbox1 = toga.Box(style=horizontal_box_pack)
edit_book_hbox2 = toga.Box(style=horizontal_box_pack)
edit_book_vbox0 = toga.Box(style=vertical_box_pack)

# Edit book Widgets
edit_book_label = toga.Label("Edit book", style=label_pack)
edit_book_retrieve_button = toga.Button("Retrieve", style=large_button_pack)
edit_book_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
edit_book_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)
edit_book_isbn_input = toga.TextInput(placeholder=ISBN_TEXT, style=text_pack)
edit_book_title_text = toga.TextInput(style=text_pack, placeholder='Title')
edit_book_authors_text = toga.TextInput(style=text_pack, placeholder='Authors')
edit_book_lccn_text = toga.TextInput(style=text_pack, placeholder='LCCN')
edit_book_lcc_text = toga.TextInput(style=text_pack, placeholder='LCC')


# Delete book Form==========================================================

# Delete book Boxes
delete_book_fill_box = toga.Box(style=Pack(flex=1))
delete_book_hbox0 = toga.Box(style=horizontal_box_pack)
delete_book_hbox1 = toga.Box(style=horizontal_box_pack)
delete_book_hbox2 = toga.Box(style=horizontal_box_pack)
delete_book_hbox3 = toga.Box(style=horizontal_box_pack)

# Delete book Widgets
delete_book_label = toga.Label("Delete book", style=label_pack)
delete_book_isbn_input = toga.TextInput(placeholder=ISBN_TEXT, style=small_text_pack)
delete_book_copy_no = toga.TextInput(placeholder=COPY_TEXT, style=copy_no_text_pack)
delete_book_all_switch = toga.Switch("Delete all books matching ISBN", style=switch_pack)
delete_book_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
delete_book_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)


# Add student Form============================================================

# Add student Boxes
add_student_fill_box = toga.Box(style=Pack(flex=1))
add_student_hbox0 = toga.Box(style=horizontal_box_pack)
add_student_hbox1 = toga.Box(style=horizontal_box_pack)
add_student_hbox2 = toga.Box(style=horizontal_box_pack)
add_student_hbox3 = toga.Box(style=horizontal_box_pack)
add_student_hbox4 = toga.Box(style=horizontal_box_pack)

# Add student Widgets
add_student_label = toga.Label("Add student", style=label_pack)
add_student_student_input = toga.TextInput(placeholder=STUDENT_TEXT, style=text_pack)
add_student_first_name_input = toga.TextInput(placeholder="Enter the student's first name", style=text_pack)
add_student_last_name_input = toga.TextInput(placeholder="Enter the student's last name", style=text_pack)
add_student_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
add_student_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)


# Retrieve student Form=======================================================

# Retrieve student Boxes
retrieve_student_fill_box = toga.Box(style=Pack(flex=1))
retrieve_student_hbox0 = toga.Box(style=horizontal_box_pack)
retrieve_student_hbox1 = toga.Box(style=horizontal_box_pack)
retrieve_student_hbox2 = toga.Box(style=horizontal_box_pack)

# Retrieve student Widgets
retrieve_student_label = toga.Label("Retrieve student", style=label_pack)
retrieve_student_student_input = toga.TextInput(placeholder=STUDENT_TEXT, style=text_pack)
retrieve_student_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
retrieve_student_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)


# Edit student Form===========================================================

# Edit student Boxes
edit_student_fill_box = toga.Box(style=Pack(flex=1))
edit_student_hbox0 = toga.Box(style=horizontal_box_pack)
edit_student_hbox1 = toga.Box(style=horizontal_box_pack)
edit_student_hbox2 = toga.Box(style=horizontal_box_pack)
edit_student_vbox0 = toga.Box(style=vertical_box_pack)

# Edit student Widgets
edit_student_label = toga.Label("Edit student", style=label_pack)
edit_student_retrieve_button = toga.Button("Retrieve", style=large_button_pack)
edit_student_student_input = toga.TextInput(placeholder=STUDENT_TEXT, style=text_pack)
edit_student_first_name_input = toga.TextInput(placeholder="First name", style=text_pack)
edit_student_last_name_input = toga.TextInput(placeholder="Last name", style=text_pack)
edit_student_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
edit_student_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)


# Delete student Form=========================================================

# Delete student Boxes
delete_student_fill_box = toga.Box(style=Pack(flex=1))
delete_student_hbox0 = toga.Box(style=horizontal_box_pack)
delete_student_hbox1 = toga.Box(style=horizontal_box_pack)
delete_student_hbox2 = toga.Box(style=horizontal_box_pack)

# Delete student Widgets
delete_student_label = toga.Label("Delete student", style=label_pack)
delete_student_student_input = toga.TextInput(placeholder=STUDENT_TEXT, style=text_pack)
delete_student_submit_button = toga.Button(SUBMIT_TEXT, style=button_pack)
delete_student_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)


# Database sort Form==========================================================

# Database sort Boxes
database_sort_fill_box = toga.Box(style=Pack(flex=1))
database_sort_hbox0 = toga.Box(style=horizontal_box_pack)
database_sort_hbox1 = toga.Box(style=Pack(direction=ROW, flex=1))
database_sort_hbox2 = toga.Box(style=horizontal_box_pack)

# Database sort Widgets
database_sort_label = toga.Label("Database sort", style=Pack(text_align=CENTER, flex=1, padding=8))
database_sort_cancel_button = toga.Button(CANCEL_TEXT, style=button_pack)
database_sort_table = toga.Table(['Heading'], style=Pack(flex=1))