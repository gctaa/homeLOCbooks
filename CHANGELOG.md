ICBD Application
----------------

1.1.0
=====

- Add support for ISBNs ending with X (inherted from ICBD Lib)
- Rename CHANGELOG to CHANGELOG.md to allow for Markdown formatting
- Remove the sort options from Database Sort View and set LCC Sort
  as the default sorting method
- Increase padding on the label in Database Sort View
- Implement Retrieve Book's Loaned By section with student ID and
  check out date
- Implement Retrieve Student's Loans section with ISBN, copy number,
  and check out date
- Fix an issue that caused the copy number text field to not empty
  itself after a successful retrieval in Retrieve Book
- Fix an issue that caused the copy number text field to not disable
  itself when submitting a request


1.0.1
=====

- Fix an issue that caused the ISBN and the LCC to be reversed in the Database
  Sort view