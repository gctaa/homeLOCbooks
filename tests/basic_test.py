# Test file of basic package functions that does not guarantee that the
# package is fully working.

import sys
import os.path
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
import time


print(f'{time.time()} TEST STARTED')
import icbd_lib

# Create a book instance
my_book = icbd_lib.Book('9781562761332')

# Gather data
my_book.gather()

# Set the copy number
my_book.set_copy_no(1)

# Check attributes
if not my_book.title == 'How software works':
    print('Incorrect title')
if not my_book.authors == 'White, Ron':
    print('Incorrect authors')
if not my_book.isbn == '9781562761332':
    print('Incorrect isbn')
if not my_book.lccn == '94120941':
    print('Incorrect lccn')
if not my_book.lcc == 'QA76.754.W49':
    print('Incorrect lcc')
if not my_book.copy_no == 1:
    print('Incorrect copy_no')
if not my_book.loaned_by == 'N/A (IMPLEMENT THIS ALREADY!!!)':
    print('Incorrect loaned_by')

# Create a student instance
my_student = icbd_lib.Student(1000000, 'First Name', 'Last Name')

# Check attributes
if not my_student.student_id == 1000000:
    print('Incorrect student id')
if not my_student.first_name == 'First Name':
    print('Incorrect first name')
if not my_student.last_name == 'Last Name':
    print('Incorrect last name')

# Create database instance
my_database = icbd_lib.Database()

# Create a book entry
my_database.create_book(my_book)

# Retrieve a book entry
my_book_ret = my_database.retrieve_book('9781562761332', 1)

# Check if retrieved data is correct
if not my_book_ret.title == 'How software works':
    print('Incorrect title')
if not my_book_ret.authors == 'White, Ron':
    print('Incorrect authors')
if not my_book_ret.isbn == '9781562761332':
    print('Incorrect isbn')
if not my_book_ret.lccn == '94120941':
    print('Incorrect lccn')
if not my_book_ret.lcc == 'QA76.754.W49':
    print('Incorrect lcc')
if not my_book_ret.copy_no == 1:
    print('Incorrect copy_no')
if not my_book_ret.loaned_by == 'N/A (IMPLEMENT THIS ALREADY!!!)':
    print('Incorrect loaned_by')

print(f'{time.time()} TEST FINISHED')
