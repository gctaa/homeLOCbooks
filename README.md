Home Library of Congress Book Organizing System
===============================================

About
-----

This is a project of students at the
[Arlington Career Center](https://careercenter.apsva.us/) in Arlington,
Virginia. The immediate practical goal is to create a CS/IT lending library
for use by our students.

Among the skills / learning objectives we are sure to discover through this
project are:

* APIs
* RESTful APIs
* Web APIs
* Using Python to access public data through publicly available APIs.
* Deeper understanding of Python dictionaries
* SQL / SQLite
* Parsing strings / sanitizing data
* Designing a simple database

The plan is that if we can successfully turn this into a useful tool, we will
reward ourselves and celebrate by making a trip to the increadible
[Library of Congress](https://www.loc.gov), which is only a short metro ride
away.

> Jeff Elkner  
> Arlington, Virginia

Resources
---------

* [LOC Labs](https://labs.loc.gov/)
* [LOC JSON API](https://libraryofcongress.github.io/data-exploration/)
* [MARC Distribution Services](https://www.loc.gov/cds/products/marcDist.php)
* [MARC Open Access API](https://www.programmableweb.com/api/library-congress-marc-open-access)
* [Google Books APIs](https://developers.google.com/books/)
* [dymoPrintLCCN.applescript](https://gist.github.com/Altece/90c77f9af6824172c05a)
* [OCLC](https://www.oclc.org/en/home.html)
* [WorldCat](https://www.worldcat.org)  
* [Koha](http://www.koha.org)
* [The LCCN Namespace](https://www.loc.gov/marc/lccn-namespace.html)
* [Anatomy of a Library of Congress Call Number](https://www.usg.edu/galileo/skills/unit03/libraries03_04.phtml)
* [Finding Books Using Library of Congress Classification](https://library.csun.edu/ResearchAssistance/LCC)
* [How to Verify and ISBN](https://www.instructables.com/id/How-to-verify-a-ISBN)
* [Sorting HOW TO](https://docs.python.org/3/howto/sorting.html)
* [Teaching with Primary Sources Internship at the Young Readers Center](https://www.loc.gov/item/internships/young-readers-center-volunteer-internship-program)
* [OCLC](https://www.oclc.org/en/home.html)
* [Open Library Books API](https://openlibrary.org/dev/docs/api/books)
* [Open Library Developers / API](https://openlibrary.org/developers/api)
