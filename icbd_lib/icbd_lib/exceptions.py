# Exceptions and warnings handler


class InvalidISBN(Exception):
    """
    Exception raised when the ISBN is invalid according to
    a formula
    """


class NoConnections(Exception):
    """
    Exception raised when there are no databases to gather
    data from (connecting failed)
    """


class InvalidClass(Exception):
    """
    Exception raised when the passed argument is not the
    correct class instance
    """


class NotEnoughInformation(Exception):
    """
    Exception raised when querying for a book without
    enough information
    """


class SQLException(Exception):
    """
    Exception raised when the SQL interpreter throws an
    exception
    """


class StudentExists(Exception):
    """
    Exception raised when the student that is being created
    already exists in the database
    """


class LoanNotFound(Exception):
    """
    Exception raised when the specified loan couldn't be
    found in the database
    """


class StudentNotFound(Exception):
    """
    Warning raised when the student with the specified ID
    has not been found in the database
    """


class LoanExists(Exception):
    """
    Exception raised when trying to loan out an already
    loaned book
    """


class TooMuchInformation(Exception):
    """
    Exception raised when some of the requested data is found
    but some characteristics of it are not met
    """


class InUse(Exception):
    """
    Exception raised when trying to delete a book or a student
    that is currently in use (loans)
    """


class BookNotFound(Warning):
    """
    Warning raised when the book with the specified ISBN
    has not been found in the database
    """


class IncompleteData(Warning):
    """
    Warning raised when the book data is incomplete
    """
