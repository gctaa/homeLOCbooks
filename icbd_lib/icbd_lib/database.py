# SQLite3 integration.


import sqlite3
import time
from .config import DATABASE_FILE
from .config import ADD_BOOK_DATA, SELECT_BOOK_DATA, DELETE_BOOK_DATA
from .config import ADD_STUDENT_DATA, SELECT_STUDENT_DATA, DELETE_STUDENT_DATA
from .config import ADD_LOAN_DATA, SELECT_LOAN_DATA, DELETE_LOAN_DATA
from .config import CREATE_BOOKS_TABLE, CREATE_STUDENTS_TABLE, CREATE_LOANS_TABLE
from .config import SELECT_LOAN_DATA_ISBN, SELECT_LOAN_DATA_STUDENT
from .config import SORT_BY_LCC, SORT_BY_TITLE, SORT_BY_ID, SORT_BY_NAME
from .classes import Book, Student, Loan
from .helpers import write_to_log
from .exceptions import InvalidClass, BookNotFound, NotEnoughInformation, TooMuchInformation
from .exceptions import StudentExists, StudentNotFound, LoanNotFound, LoanExists, InUse


# Connect and use a database.
class Database:
    def __init__(self):
        self.__con = sqlite3.connect(DATABASE_FILE)
        self.__cur = self.__con.cursor()
        self.__cur.executescript(CREATE_BOOKS_TABLE)
        self.__cur.executescript(CREATE_STUDENTS_TABLE)
        self.__cur.executescript(CREATE_LOANS_TABLE)
        self.commit()

    # Commit changes.
    def commit(self):
        write_to_log('DATA: Committing changes to database...')
        self.__con.commit()

    # Create book row in database.
    def create_book(self, book):
        write_to_log('DATA: Adding book to database...')
        if not isinstance(book, Book):
            raise InvalidClass
        try:
            self.retrieve_book(book.isbn)
            write_to_log('DATA: Book found; creating a row with '
                         'copy_no = 2...')
            self.__cur.execute(ADD_BOOK_DATA, (book.title, book.authors,
                                               book.lccn, book.isbn,
                                               book.lcc, 2))
        except BookNotFound:
            write_to_log('DATA: Book not found; creating a row with '
                         'copy_no = 1...')
            self.__cur.execute(ADD_BOOK_DATA, (book.title, book.authors,
                                               book.lccn, book.isbn,
                                               book.lcc, 1))
        except NotEnoughInformation:
            self.__cur.execute(SELECT_BOOK_DATA, (book.isbn,))
            copy_no = len(self.__cur.fetchall())
            write_to_log('DATA: Book found; creating a row with '
                         'copy_no incremented...')
            self.__cur.execute(ADD_BOOK_DATA, (book.title, book.authors,
                                               book.lccn, book.isbn,
                                               book.lcc, copy_no + 1))
        self.commit()

    # Get book row from database.
    def retrieve_book(self, isbn, copy_no=None):
        write_to_log('DATA: Retrieving book from database...')
        self.__cur.execute(SELECT_BOOK_DATA, (isbn, ))
        book = self.__cur.fetchall()
        if len(book) == 0:
            write_to_log(f'DATA: No data found for book with ISBN {isbn}.')
            raise BookNotFound
        if (len(book) == 1 and copy_no is None) or (len(book) == 1 and copy_no == 1):
            book_instance = Book(isbn)
            book_instance.title = book[0][0]
            book_instance.authors = book[0][1]
            book_instance.lccn = book[0][2]
            book_instance.lcc = book[0][4]
            book_instance.copy_no = int(book[0][5])
            return book_instance
        if len(book) == 1 and copy_no != 1:
            write_to_log(f'DATA: Data was found for book with ISBN {isbn}, but didn\'t match the copy number.')
            raise TooMuchInformation
        if len(book) > 1 and copy_no is not None:
            for i in range(len(book)):
                if book[i][5] == copy_no:
                    book_instance = Book(isbn)
                    book_instance.title = book[0][0]
                    book_instance.authors = book[0][1]
                    book_instance.lccn = book[0][2]
                    book_instance.lcc = book[0][4]
                    book_instance.copy_no = int(book[0][5])
                    return book_instance
            raise BookNotFound
        else:
            raise NotEnoughInformation

    # Edit book row in database.
    def edit_book(self, book):
        write_to_log('DATA: Editing book data...')
        if not isinstance(book, Book):
            raise InvalidClass
        self.__cur.execute(SELECT_BOOK_DATA, (book.isbn, ))
        books = self.__cur.fetchall()
        if len(books) == 0:
            raise BookNotFound
        else:
            i = 1
            for _ in books:
                self.__cur.execute(DELETE_BOOK_DATA, (book.isbn, i))
                self.__cur.execute(ADD_BOOK_DATA, (book.title, book.authors,
                                                   book.lccn, book.isbn,
                                                   book.lcc, i))
                i += 1
        self.commit()

    # Delete book row in database.
    def delete_book(self, isbn, copy_no=None, all=False):
        write_to_log('DATA: Deleting book from database...')
        self.__cur.execute(SELECT_BOOK_DATA, (isbn, ))
        books = self.__cur.fetchall()
        if all:
            if len(books) == 0:
                write_to_log(f'DATA: No data found for book with ISBN {isbn}.')
                raise BookNotFound
            else:
                i = 1
                for _ in books:
                    try:
                        self.find_loan_by_isbn_copy(isbn, i)
                        raise InUse
                    except LoanNotFound:
                        self.__cur.execute(DELETE_BOOK_DATA, (isbn, i))
                        i += 1
        else:
            if len(books) == 0:
                write_to_log(f'DATA: No data found for book with ISBN {isbn}.')
                raise BookNotFound
            elif copy_no is None:
                raise NotEnoughInformation
            else:
                try:
                    self.find_loan_by_isbn_copy(isbn, copy_no)
                    raise InUse
                except LoanNotFound:
                    i = 0
                    check = False
                    for _ in books:
                        if str(books[i][5]) == str(copy_no):
                            self.__cur.execute(DELETE_BOOK_DATA, (isbn, copy_no))
                            check = True
                        i += 1
                    if check == False:
                        raise BookNotFound
        self.commit()

    # Create student row in database.
    def create_student(self, student):
        write_to_log('DATA: Adding student to the database...')
        if not isinstance(student, Student):
            raise InvalidClass
        try:
            self.retrieve_student(student.student_id)
            raise StudentExists
        except StudentNotFound:
            write_to_log('DATA: Student not found; creating a row...')
            self.__cur.execute(ADD_STUDENT_DATA, (student.student_id,
                                                  student.first_name,
                                                  student.last_name))
        self.commit()

    # Get student row in database and books checked out.
    def retrieve_student(self, student_id):
        write_to_log('DATA: Retrieving student from database...')
        self.__cur.execute(SELECT_STUDENT_DATA, (student_id,))
        student = self.__cur.fetchall()
        if len(student) == 0:
            write_to_log(f'DATA: No data found for '
                         f'student with ID {student_id}.')
            raise StudentNotFound
        elif len(student) == 1:
            student_instance = Student(student[0][0], student[0][1], student[0][2])
            return student_instance

    # Edit student row in database.
    def edit_student(self, student):
        write_to_log('DATA: Editing student data...')
        if not isinstance(student, Student):
            raise InvalidClass
        self.__cur.execute(SELECT_STUDENT_DATA, (student.student_id, ))
        students = self.__cur.fetchall()
        if len(students) == 0:
            raise StudentNotFound
        else:
            self.__cur.execute(DELETE_STUDENT_DATA, (student.student_id, ))
            self.__cur.execute(ADD_STUDENT_DATA, (student.student_id,
                                                  student.first_name,
                                                  student.last_name))
        self.commit()

    # Delete student row in database.
    def delete_student(self, student_id):
        write_to_log('DATA: Deleting student from database...')
        try:
            self.find_loans_by_student_id(student_id)
            raise InUse
        except LoanNotFound:
            self.__cur.execute(SELECT_STUDENT_DATA, (student_id, ))
            student = self.__cur.fetchall()
            if len(student) == 0:
                raise StudentNotFound
            else:
                self.__cur.execute(DELETE_STUDENT_DATA, (student_id, ))
        self.commit()

    # Create a loan row in database and associate it with Student ID.
    def check_out(self, student_id, isbn, copy_no):
        write_to_log('DATA: Checking book out...')
        self.retrieve_student(student_id)
        self.retrieve_book(isbn, copy_no=copy_no) # May throw BookNotFound, TooMuchInformation, NotEnoughInformation
        self.__cur.execute(SELECT_LOAN_DATA_ISBN, (isbn, copy_no))
        loans = self.__cur.fetchall()
        current_time = time.strftime("%x %X")
        if len(loans) == 0:
            self.__cur.execute(ADD_LOAN_DATA, (isbn, copy_no, student_id,
                                               current_time))
        else:
            raise LoanExists
        self.commit()

    # Update a loan row in database and remove association with Student ID.
    def check_in(self, student_id, isbn, copy_no):
        write_to_log('DATA: Checking book in...')
        self.retrieve_student(student_id)
        self.retrieve_book(isbn, copy_no=copy_no) # May throw BookNotFound, TooMuchInformation, NotEnoughInformation
        self.__cur.execute(SELECT_LOAN_DATA_STUDENT, (student_id, ))
        loans = self.__cur.fetchall()
        if len(loans) == 0:
            raise LoanNotFound
        else:
            for loan in loans:
                if (loan[0] == isbn) and (loan[1] == int(copy_no)):
                    self.__cur.execute(DELETE_LOAN_DATA, (isbn, copy_no))
                    break
        self.commit()

    # Find all loans of a student.
    def find_loans_by_student_id(self, student_id):
        write_to_log('DATA: Finding the loans of a student...')
        self.__cur.execute(SELECT_LOAN_DATA_STUDENT, (student_id,))
        loans = self.__cur.fetchall()
        if len(loans) == 0:
            raise LoanNotFound
        else:
            loan_instances = []
            for loan in loans:
                loan_instances.append(Loan(loan[0], loan[1], loan[2], loan[3]))
        return loan_instances

    # Find borrower of a book.
    def find_loan_by_isbn_copy(self, isbn, copy_no):
        write_to_log('DATA: Finding the borrower of a book...')
        if copy_no is None:
            copy_no = 1
        self.__cur.execute(SELECT_LOAN_DATA_ISBN, (isbn, copy_no))
        loan = self.__cur.fetchall()
        if len(loan) == 0:
            raise LoanNotFound
        elif len(loan) == 1:
            return Loan(loan[0][0], loan[0][1], loan[0][2], loan[0][3])
        else:
            raise Exception('What is going on? '
                            'How can a book be loaned to 2 students!?')
    
    # Sort books by LCC
    def sort_by_lcc(self):
        write_to_log('DATA: Sorting by LCC...')
        self.__cur.execute(SORT_BY_LCC)
        sorted_books = self.__cur.fetchall()
        book_instances = []
        for book in sorted_books:
            _ = Book(book[3])
            _.title = book[0]
            _.authors = book[1]
            _.lccn = book[2]
            _.lcc = book[4]
            _.copy_no = book[5]
            book_instances.append(_)
        return book_instances
