# Functions that are useful in multiple files.


from .config import DATABASES, LOG_FILE
import platform
import subprocess as sp
from multiprocessing.pool import ThreadPool
from pyaz import zoom


# Open Log file.
if LOG_FILE != '':
    log_file = open(LOG_FILE, 'a')
    import time
    log_file.writelines('\nICBD LIBRARY LOADED\n')


# Write to log file
def write_to_log(text):
    if text != '' and text is not None:
        if LOG_FILE != '':
            current_time = time.strftime("%x %X")
            log_file.writelines(f'{current_time} | {text}\n')


# Establish a connection to a single database.
def connect(db):
    write_to_log(f'HELP: Connecting to {db[0]}')
    try:
        return zoom.Connection(db[0], db[1],
                               databaseName=db[2], timeout=5,
                               sru=None, extraArgs='')
    except (ConnectionError, TimeoutError) as e:
        write_to_log(f'HELP WARNING: Connecting failed ({db[0]}) - {e}')
        return None


# Connections to servers.
def connect_to_dbs():
    write_to_log('HELP: Establishing connections to databases...')
    pool = ThreadPool(len(DATABASES))
    return list(pool.map(connect, DATABASES))


# Check if ISBN is valid.
def is_isbn_valid(isbn):
    isbn = isbn if type(isbn) is str else str(isbn)
    if len(isbn) == 13:
        if not isbn.isdigit():
            return False
        checksum = 0
        one_three = 1
        for digit in isbn:
            checksum += one_three * int(digit)
            one_three = 1 if one_three == 3 else 3
        return checksum % 10 == 0
    if len(isbn) == 10:
        if not isbn.isdigit():
            if not isbn[-1].isdigit():
                return False
        checksum = 0
        for offset, value in enumerate(isbn):
            if value == 'X' or value == 'x':
                checksun += (10 - offset) * int (10)
            else:
                checksum += (10 - offset) * int(value)
        return checksum % 11 == 0
    return False


# Clear the console screen.
def clear_screen():
    if platform.system() == 'Linux':
        _ = sp.call('clear', shell=True)
    else:
        _ = sp.call('cls', shell=True)
    return True
