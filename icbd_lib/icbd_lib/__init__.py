# Initiate the package


print('Loading module ICBD...')


from .classes import Book, Student, Loan
from .classes import close_connections
from .database import Database
from .exceptions import *
