# Store variables that are used in this package.


# Config variables
DATABASE_FILE = 'library.db'
LOG_FILE = 'icbd.log'


# Predefined servers' urls, ports, and database names.
DATABASES = [('z3950.loc.gov', 7090, 'voyager',),
             ('lx2.loc.gov', 210, 'LCDB',),
             ('library.mit.edu', 9909, 'mit01pub',),
             ('albert.rit.edu', 210, 'innopac',),
             ('vmvm.alc.org', 2200, 'public'),
             ('catalog.lib.utexas.edu', 210, 'innopac'),
             ]


# Preferred Record Syntax.
PREF_REC_SYNTAX = 'USMARC'


# Query Type.
QUERY_TYPE = 'PQF'


# SQL Class Statements
ADD_BOOK_DATA = 'INSERT INTO books(title, authors, lccn, ' \
                'isbn, lcc, copy_no) VALUES(?, ?, ?, ?, ?, ?)'
SELECT_BOOK_DATA = 'SELECT * FROM books WHERE isbn=?'

DELETE_BOOK_DATA = 'DELETE FROM books WHERE isbn=? AND copy_no=?'


ADD_STUDENT_DATA = 'INSERT INTO students(student_id, first_name, last_name)' \
                   ' VALUES(?, ?, ?)'
SELECT_STUDENT_DATA = 'SELECT * FROM students WHERE student_id=?'

DELETE_STUDENT_DATA = 'DELETE FROM students WHERE student_id=?'


ADD_LOAN_DATA = 'INSERT INTO loans(isbn, copy_no, student_id, ' \
                'date_out) VALUES(?, ?, ?, ?)'

SELECT_LOAN_DATA = 'SELECT * FROM loans WHERE isbn=? ' \
                   'AND copy_no=? AND student_id=?'

SELECT_LOAN_DATA_STUDENT = 'SELECT * FROM loans WHERE student_id=?'
SELECT_LOAN_DATA_ISBN = 'SELECT * FROM loans WHERE isbn=? AND copy_no=?'

DELETE_LOAN_DATA = 'DELETE FROM loans WHERE isbn=? AND copy_no=?'


# SQL Tables Statements
CREATE_BOOKS_TABLE = 'CREATE TABLE IF NOT EXISTS books (' \
                     'title   	TEXT,' \
                     'authors		TEXT,' \
                     'lccn     	TEXT,' \
                     'isbn		TEXT NOT NULL,' \
                     'lcc			TEXT,' \
                     'copy_no		INT NOT NULL,' \
                     'CONSTRAINT books_pk PRIMARY KEY (isbn, copy_no)' \
                     ');'
CREATE_STUDENTS_TABLE = 'CREATE TABLE IF NOT EXISTS students(' \
                        'student_id	INT NOT NULL,' \
                        'first_name	TEXT,' \
                        'last_name	TEXT,' \
                        'CONSTRAINT students_pk PRIMARY KEY (student_id)' \
                        ');'
CREATE_LOANS_TABLE = 'CREATE TABLE IF NOT EXISTS loans(' \
                     'isbn		TEXT,' \
                     'copy_no 	INT,' \
                     'student_id	INT,' \
                     'date_out	TEXT,' \
                     'FOREIGN KEY(student_id) ' \
                     'REFERENCES students(student_id)' \
                     ');'

# SQL Sort Statements
SORT_BY_LCC = "SELECT * FROM books ORDER BY lcc DESC"
SORT_BY_TITLE = ''
SORT_BY_ID = ''
SORT_BY_NAME = ''