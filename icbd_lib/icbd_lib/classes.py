# Book, Student, and Loan classes are defined here.


from .helpers import *
from .config import PREF_REC_SYNTAX, QUERY_TYPE
from .exceptions import InvalidISBN, IncompleteData, NoConnections
import pymarc
from pyaz import zoom
import random

connections = connect_to_dbs()


# Gather and keep data about the book.
class Book:
    def __init__(self, isbn):
        write_to_log(f'LOG: Initializing Book instance with ISBN: {isbn}')
        self.title = ""
        self.authors = ""
        self.lcc = ""
        self.lccn = ""
        self.isbn = isbn if type(isbn) is str else str(isbn)
        self.copy_no = 1
        self.__attributes_check = ('title', 'authors', 'lcc', 'lccn', 'isbn')
        if not is_isbn_valid(self.isbn):
            write_to_log(f'EXPT: ISBN: {isbn} is invalid')
            raise InvalidISBN

    # A wrapper for __get_data.
    def gather(self):
        write_to_log(f'LOG: Gathering book data')
        is_data_complete = self.__get_data()
        if not is_data_complete:
            write_to_log(f'WARN: Incomplete data for ISBN: {self.isbn}')
            raise IncompleteData

    # Get book data from Z39.50 servers.
    def __get_data(self):
        random.shuffle(connections)
        if connections is None:
            raise NoConnections
        for connection in connections:
            if connection is None:
                continue
            query = zoom.Query(QUERY_TYPE, f'@attr 1=7 {self.isbn}')
            try:
                result = connection.search(query,
                         preferredRecordSyntax=PREF_REC_SYNTAX)
            except Exception as e:
                print(f'DATA EXCEPTION: {e} - '
                      f'Skipping {connection.host}.')
                continue
            if len(result) > 0:
                try:
                    reader = list(pymarc.MARCReader(
                        list(result)[0].get('raw')))
                except Exception as e:
                    print(f'DATA EXCEPTION: {e} - '
                          f'Skipping {connection.host}.')
                    continue
                self.__add_data_to_class(reader)
            else:
                write_to_log(f'WARN: Cannot find book at {connection.host}!')
            if self.__check_if_all_keys_are_filled():
                self.__format()
                return True
        self.__format()
        return False

    # Add data from the MARC reader to the class instance attributes.
    def __add_data_to_class(self, reader):
        for record in reader:
            if self.title == '' and record.title() is not None:
                self.title = record.title()
            if self.authors == '' and record.author() is not None:
                self.authors = record.author()
            if self.lccn == '':
                try:  # LCCN *MAY* be provided in fields 001 and 003.
                    self.lccn = record.get_fields('010')[0].value()
                except IndexError:  # IndexErrors mean data is unavailable.
                    pass
                if self.lcc == '':
                    try:
                        self.lcc = record.get_fields('050')[0].value()
                    except IndexError:
                        pass

    # Format the attributes of a class.
    def __format(self):
        self.title = self.__single_format(self.title)
        self.authors = self.__single_format(self.authors)
        self.lcc = self.__single_format(self.lcc, kind='lcc')
        self.lccn = self.__single_format(self.lccn)

    # Format a single attribute.
    def __single_format(self, attr, kind=None):
        attr = str(attr)
        if attr.endswith('.'):
            attr = attr[:-1]
        if attr.endswith('\\'):
            attr = attr[:-1]
        if attr.endswith('-'):
            attr = attr[:-1]
        try:
            if not attr.isnumeric():
                while attr[-1].isdigit():
                    attr = attr[:-1]
        except IndexError:
            pass
        if attr.endswith(' '):
            attr = attr[:-1]
        if attr.endswith(', author.'):
            attr = attr[:-9]
        if attr.endswith(' /'):
            attr = attr[:-2]
        if attr.endswith(' //r'):
            attr = attr[:-4]
        if attr.endswith(','):
            attr = attr[:-1]
        if kind == 'lcc':
            attr = attr.replace(" ", "")
        return attr

    # Check if all the attributes of a class are populated.
    def __check_if_all_keys_are_filled(self):
        for attribute_name in self.__attributes_check:
            attribute = getattr(self, attribute_name)
            if attribute == '' or attribute is None:
                return False
        return True


# Keep data about the student.
class Student:
    def __init__(self, student_id, first_name, last_name):
        self.student_id = student_id if type(student_id) \
                                        is int else int(student_id)
        self.first_name = first_name if type(first_name) \
                                        is str else str(first_name)
        self.last_name = last_name if type(last_name) \
                                      is str else str(last_name)
        self.loaned_books = 'N/A (IMPLEMENT THIS ALREADY!!!)'

    # Set the loaned_books variable.
    def set_loaned_books(self, isbn_copy_tuple):
        self.loaned_books = isbn_copy_tuple


# Keep data about a loan.
class Loan:
    def __init__(self, isbn, copy_no, student_id, date_out):
        self.isbn = isbn if str(isbn) is str else str(isbn)
        self.copy_no = copy_no if type(copy_no) is int else int(copy_no)
        self.student_id = student_id if type(student_id) \
                                        is int else int(student_id)
        self.date_out = date_out if type(date_out) is str else str(date_out)


# Close the connections
def close_connections():
    for connection in connections:
        del connection
