#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='icbd_lib',
    version='1.1.0',
    description='A library system that includes a fully functional book organizing, lending and student system.',
    url='https://gitlab.com/gctaa/homeLOCbooks',
    author='GCTAA',
    author_email='ionut.barbu@outlook.com',
    license='GNU General Public License v3 or later (GPLv3+)',
    packages=find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
    ],
    install_requires=[
    ],
    options={
        'app': {
            'formal_name': 'ICBD Library',
            'bundle': 'com.gctaa'
        }
    }
)
