Documentation
=============

## Notice

Documentation about the classes, functions and some variables is found in the
source code. The classes and functions that are exposed and **should** be used
are listed below.

## Installation

LINUX: This package **requires** `libyaz5` from apt.
MACOS: This package **requires** `yaz` from hombrew.

You can then use `pip install -r requirements.txt` to install the module.

### Warning

This repository contains 2 projects:
* `icbd_lib` is a package which contains tools for creating an application that gathers
book data and puts it in a database along with a student and loan system
* `icbd_app` is a GUI application based on `icbd_lib` that requires BeeWare

`icbd_app` uses [BeeWare](https://github.com/beeware/) and will attempt
to install `beeware-toga`. Make sure you have all the required dependencies if
you plan on using `icbd_app`.

## Configuration

There are 2 configuration options in the file `config.py` in the package root.
The first line `DATABASE_FILE` is the name of the database file that will be created
when running the package. The second line, `LOG_FILE` represents the name of the file
where log data will be saved. An empty filename will not create a log file.


## API

Some of the the functions raise more exceptions than shown here. Unfortunately, the DOCS
have not been updated yet.

### Classes

#### Book Class

`Book` is a class that is initialized with a string that contains the book's
ISBN and the exception `InvalidISBN` may be raised if the passed argument is
not a valid ISBN.

* `gather()` is used to request data gathering for the `Book` class and it may
raise the exception `NoConnections` if the package cannot connect to the Z39.50
servers. The warning `IncompleteData` will also most likely be risen when the
data gathered is incomplete.

* `set_loaned_by(student_id)` accepts a Student ID that is assigned to the
respective Book instance.

* `set_copy_no(copy_no)` is used to set the copy number of a `Book` instance.

#### Student Class

`Student` is a class that is initialized with an integer that represents the
 Student ID and 2 strings that contain their first and last name.

* `set_loaned_books(isbn_copy_tuple)` is used to assign the books a student has
loaned to the Student instance. It has one argument, a tuple of tuples that
contain an ISBN and a copy number, used to identify the books in the database.

#### Loan Class

`Loan` is a class that is initialized by the API and should only be used for
displaying information.

#### Database Class

`Database` is a class that is initialized with no parameters and creates a
connection to the local database.

##### Book Related Operations

* `create_book(book)` create a new row in the database with the information
provided by the Book instance that is passed as an argument. It may return a
`InvalidClass` exception in case the wrong class type is passed.

* `retrieve_book(isbn, copy_no=None)` retrieves book data and returns a class.
It is called with an ISBN and an optional copy number. In case there are
multiple books with the same ISBN, the exception `NotEnoughInformation` will
be raised; in this situation, specify a value for `copy_no`. `BookNotFound`
may also be raised in case a book with the specified parameters is not found.

* `edit_book(book)` accepts a `Book` instance as an argument and edits the
data of the book that has the same ISBN and copy number as the passed
argument. It may raise `InvalidClass` if the passed argument is not a `Book`
instance.

* `delete_book(isbn, copy_no=None)` deletes a book row. In case multiple rows
are found with the same ISBN, the function will raise `NotEnoughInformation`
and a `copy_no` will be required for the deletion of the book. `BookNotFound`
may also be raised if a book meeting the passed arguments could not be found.

##### Student Related Operations

* `create_student(student, force=False)` creates a new student row in the
database. The possible exceptions are `InvalidClass` which is raised when
`Student` is not passed and `StudentExists` which is raised when attempting
to create a student with an ID that is already in use.

* `retrieve_student(student_id)` retrieves data about the student with passed
ID and returns an appropriate class. It is called with a Student ID and may
throw a `StudentNotFound` warning if a student with the respective ID has
not been found.

* `edit_student(student)` is used to edit the data of an existing student.
It may raise `StudentNotFound` if the student with the specified ID is not
found. `InvalidClass` may also be raised if the passed argument is not a
`Student` instance.

* `delete_student()` deletes a student row for the specified ID. In case
no student with that ID is found, a `StudentNotFound` warning is raised.

##### Checking Related Operations

* `check_out(student_id, isbn, copy_no)` accept a Student ID, an ISBN, and
a copy number. It adds a loan row in the `loans` table with the above data
along with a timestamp.

* `check_in(student_id, isbn, copy_no)` returns a `Loan` instance. It may
raise `LoanNotFound` if a loan with the specified parameters could not be
found in the database.

* `find_loans_by_student_id(student_id)` finds loans by Student ID. It
returns a list of `Loan` instances. It may raise `LoanNotFound` if no
loan was found for the respective Student ID.

* `find_loan_by_isbn_copy(isbn, copy_no)` finds a loan by ISBN and copy
number. It returns a `Loan` instance. It may raise `LoanNotFound` if the
book with the respective ISBN is not loaned to anybody.

##### Extras

* `commit()` may be used for manual committing, although all the functions in
the class `Database` automatically commit the changes to the database.

### Exceptions

Documentation about exceptions can be found in the `exceptions.py` file
located in the package root.
